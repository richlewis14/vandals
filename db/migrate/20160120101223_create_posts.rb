class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :message
      t.string :picture
      t.string :link
      t.string :facebook_post_id
      t.string :large_image_url
      t.text :description
      t.string :video_url
      t.date :created_time

      t.timestamps null: false
    end
  end
end
