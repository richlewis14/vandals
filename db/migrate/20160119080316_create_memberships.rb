# Membership Class
class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.string :surname
      t.string :forename
      t.string :address
      t.string :town
      t.string :county
      t.string :postcode
      t.string :occupation
      t.date :dob
      t.string :home_phone
      t.string :business_phone
      t.string :mobile_phone
      t.string :email
      t.string :membership_type # Full or Associate
      t.string :payment_method # Cash, Standing Order, Cheque
      t.string :paid_in_full # Yes, No or N/A
      t.date :payment_last_recieved
      t.timestamps null: false
    end
  end
end
