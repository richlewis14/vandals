# Add File to Memebership Model
class AddFileToMembershipModel < ActiveRecord::Migration
  def change
    add_attachment :memberships, :file
  end

  def down
    remove_attachment :memberships, :file
  end
end
