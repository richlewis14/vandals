class AddSportIdToTournament < ActiveRecord::Migration
  def change
    add_column :tournaments, :sport_id, :integer
  end

  def down
    remove_column :tournaments, :sport_id
  end
end
