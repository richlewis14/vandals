class AddSportIdToFixture < ActiveRecord::Migration
  def change
    add_column :fixtures, :sport_id, :integer
  end

  def down
  	remove_column :fixtures, :sport_id
  end
end
