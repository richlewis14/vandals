# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160125154133) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "fixtures", force: :cascade do |t|
    t.string   "home_team"
    t.string   "away_team"
    t.time     "kickoff_time"
    t.integer  "tournament_id"
    t.integer  "home_score_result"
    t.integer  "away_score_result"
    t.date     "fixture_date"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "sport_id"
  end

  create_table "galleries", force: :cascade do |t|
    t.integer  "tournament_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "gallery_images", force: :cascade do |t|
    t.integer  "gallery_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "memberships", force: :cascade do |t|
    t.string   "surname"
    t.string   "forename"
    t.string   "address"
    t.string   "town"
    t.string   "county"
    t.string   "postcode"
    t.string   "occupation"
    t.date     "dob"
    t.string   "home_phone"
    t.string   "business_phone"
    t.string   "mobile_phone"
    t.string   "email"
    t.string   "membership_type"
    t.string   "payment_method"
    t.string   "paid_in_full"
    t.date     "payment_last_recieved"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "posts", force: :cascade do |t|
    t.text     "message"
    t.string   "picture"
    t.string   "link"
    t.string   "facebook_post_id"
    t.string   "large_image_url"
    t.text     "description"
    t.string   "video_url"
    t.date     "created_time"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "sports", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tournaments", force: :cascade do |t|
    t.string   "name"
    t.date     "tourn_date"
    t.string   "tourn_location"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "sport_id"
  end

end
