if Rails.env.development?
  Membership.destroy_all
  Tournament.destroy_all
  Fixture.destroy_all
  Gallery.destroy_all
  GalleryImage.destroy_all
  Sport.destroy_all
end

if AdminUser.count == 0
  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
end

# Create Memberships
membership_types = %w(Full Associate)
payment_methods = %w(Standing\ Order Cash Cheque)
paid_in_full = %w(Yes No Not\ Applicable)
10.times do
  Membership.create(
    surname: Faker::Name.last_name,
    forename: Faker::Name.first_name,
    address: Faker::Address.street_address,
    town: Faker::Address.city_prefix,
    county: Faker::Address.state,
    postcode: Faker::Address.postcode,
    occupation: Faker::Company.profession,
    dob: Faker::Date.between(Date.today - 365, Date.today),
    home_phone: Faker::Number.number(11),
    business_phone: Faker::Number.number(11),
    mobile_phone: Faker::Number.number(11),
    email: Faker::Internet.email,
    membership_type: membership_types.sample,
    payment_method: payment_methods.sample,
    paid_in_full: paid_in_full.sample,
    payment_last_recieved: Faker::Date.between(Date.today - 60, Date.today),
    file: File.new("#{Rails.root}/spec/fixtures/membership_form.pdf")
  )
end

# Create Sports
@sports = %w(Touch Netball)
@sports.each do |sport_name|
  Sport.create(
    name: sport_name)
end

# Create Tournaments
@touch_tournament_names = %w(WTS\ Round\ 1 DTS\ Manchester WTS\ Round\ 2 Spring\ Chicks DTS\ Lincoln)
@touch_tournament_names.each do |t|
  Tournament.create(
    sport_id: Sport.find_by_name('Touch').id,
    name: t,
    tourn_date: Faker::Date.between(Date.today - 60, Date.today),
    tourn_location: Faker::University.name
  )
end

# Create Tournaments
@netball_tournament_names = %w(Cardiff\ Netball\ League)
@netball_tournament_names.each do |t|
  Tournament.create(
    sport_id: Sport.find_by_name('Netball').id,
    name: t,
    tourn_date: Faker::Date.between(Date.today - 60, Date.today),
    tourn_location: Faker::University.name
  )
end


# Create Touch Fixtures
teams = %w(Durkas Raptors Manchester\ Chargers Gower\ Dragons)
results = %w(1 2 3 4 5 6 7 8 9)
20.times do
  Fixture.create(
    sport_id: Sport.find_by_name('Touch').id,
    tournament_id: Tournament.all.where('name != ?', 'Cardiff Netball League').sample.id,
    home_team: 'Varsity Vandals',
    away_team: teams.sample,
    kickoff_time: Faker::Time.between(0.days.ago, Time.now, :all),
    home_score_result: results.sample.to_i,
    away_score_result: results.sample.to_i,
    fixture_date: Faker::Date.between(Date.today - 60, Date.today)
  )
end

# Create Netball Fixtures
teams = %w(Netball\ Team\ 1 Netball\ Team\ 2)
results = %w(1 2 3 4 5 6 7 8 9)
20.times do
  Fixture.create(
    sport_id: Sport.find_by_name('Netball').id,
    tournament_id: Tournament.where('name = ?', 'Cardiff Netball League').first.id,
    home_team: 'Varsity Vandals',
    away_team: teams.sample,
    kickoff_time: Faker::Time.between(0.days.ago, Time.now, :all),
    home_score_result: results.sample.to_i,
    away_score_result: results.sample.to_i,
    fixture_date: Faker::Date.between(Date.today - 60, Date.today)
  )
end

# Create Galleries
@tournaments = Tournament.all
@gallery_array = %w(Lighthouse.jpg Penguins.jpg Tulips.jpg beach.jpg)
@tournaments.each do |t|
  @gallery = Gallery.create(
    tournament_id: t.id
  )
  2.times do
  gallery_array = @gallery_array.sample
    @gallery.gallery_images.create(
      photo: File.new("#{Rails.root}/spec/fixtures/#{gallery_array}")
      )
  end
end
