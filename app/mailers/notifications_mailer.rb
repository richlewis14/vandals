class NotificationsMailer < ActionMailer::Base
  default from: ENV['EMAIL_ADDRESS']
  def new_message(message)
    @message = message
    mail(to: ENV['EMAIL_ADDRESS'], subject: "Message From Varsity Vandals Website - #{@message.subject}")
  end
end
