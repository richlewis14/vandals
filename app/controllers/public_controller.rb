# Public Controller
class PublicController < ApplicationController
  def index
   # Facebook posts
  	@posts = Post.paginate(page: params[:page], per_page: 3).order('created_time DESC')
    
    # Scrolling Fixtures
    @scrolling_touch_fixtures = Fixture.joins(:sport).where('sports.name = ?', 'Touch').order('fixture_date DESC, kickoff_time DESC').limit(10)

    # All Fixtures
    @fixtures = Fixture.includes(:tournament).order('fixtures.fixture_date DESC, fixtures.kickoff_time DESC')
    @sidebar_touch_fixtures = @fixtures.joins(:sport).where('sports.name = ?', 'Touch').group_by(&:tournament)
    @touch_tournaments = Tournament.joins(:sport).where('sports.name = ?', 'Touch').order('tournaments.tourn_date DESC')
  end

  def galleries
    @tournaments = Tournament.all
    @tournament_year = @tournaments.group_by { |y| y.tourn_date.year }
  end

  def our_club
  end

  def running_club
  end

  def netball_club
    # Scrolling Fixtures
    @scrolling_netball_fixtures = Fixture.joins(:sport).where('sports.name = ?', 'Netball').order('fixture_date DESC, kickoff_time DESC').limit(10)
    @netball_tournaments = Tournament.joins(:sport).where('sports.name = ?', 'Netball').order('tournaments.tourn_date DESC')
    @fixtures = Fixture.includes(:tournament).order('fixtures.fixture_date DESC, fixtures.kickoff_time DESC')
    @sidebar_netball_fixtures = @fixtures.joins(:sport).where('sports.name = ?', 'Netball').group_by(&:tournament)
  end

  def calendar
  end
end
