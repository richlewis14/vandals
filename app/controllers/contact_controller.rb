class ContactController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
    if @message.valid?
      NotificationsMailer.new_message(@message).deliver
      redirect_to(root_path, notice: 'Thanks for your message, we will be in touch soon')
    else
      flash.now.alert = 'Error occured'
      render :new
    end
  end
end