ActiveAdmin.register Post do

  # Post Index (All Facebook Posts)
  index do
    selectable_column
    column 'Message' do |m|
      truncate(m.message.squish, length: 100)
    end	
    column 'Picture' do |p|
      image_tag(p.picture)
    end
    actions
  end

  # Show
  show do
    attributes_table do
      row :message
      row 'Picture' do |p|
      	image_tag(p.picture)
      end
      row :link
      row :facebook_post_id
      row 'Large Image' do |i|
        image_tag(i.large_image_url)
      end
      row :description
      row :video_url
    end
  end

end
