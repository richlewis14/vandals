ActiveAdmin.register Fixture do
  permit_params :home_team, :away_team, :kickoff_time, :home_score_result,
                :away_score_result, :fixture_date, :tournament_id, :sport_id
  # filters
  filter :tournament_id,
         as: :select,
         collection: Tournament.all

  filter :sport_id,
         as: :select,
         collection: Sport.all

  # Create Fixture
  form do |f|
    inputs 'Fixture Details' do
      f.semantic_errors
      f.input :tournament_id, as: :select, collection: Tournament.all
      f.input :sport_id, as: :select, collection: Sport.all
      f.input :home_team
      f.input :away_team
      f.input :kickoff_time, as: :time_picker
      f.input :home_score_result
      f.input :away_score_result
      f.input :fixture_date, as: :date_picker
      f.actions
    end
  end

  # Fixture Index (All Fixtures)
  index do
    selectable_column
    column :tournament_id do |t|
      t.tournament.name
    end
    column :sport_id do |t|
      t.sport.name
    end
    column :home_team
    column :away_team
    column :kickoff_time
    column :home_score_result
    column :away_score_result
    column :fixture_date
    actions
  end

  # Show
  show do
    attributes_table do
      row :tournament_id do |t|
        t.tournament.name
      end
      row :sport_id do |t|
        t.sport.name
      end
      row :home_team
      row :away_team
      row :kickoff_time
      row :home_score_result
      row :away_score_result
      row :fixture_date
    end
  end
end
