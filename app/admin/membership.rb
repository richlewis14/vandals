ActiveAdmin.register Membership do
  permit_params :surname, :forename, :address, :town, :county, :postcode,
                :occupation, :dob, :home_phone, :business_phone, :mobile_phone,
                :email, :membership_type, :payment_method, :paid_in_full,
                :payment_last_recieved, :file

  # Custom Controller Action
  # Download PDF
  collection_action :download_pdf, method: :get do
    membership = Membership.find(params[:id])
    send_file membership.file.path, type: 'application/pdf'
  end

  # Filters
  filter :surname
  filter :forename
  filter :membership_type
  filter :payment_method
  filter :payment_last_recieved

  # Create Membership
  form do |f|
    inputs 'Personal Details' do
      f.semantic_errors
      f.input :surname
      f.input :forename
      f.input :address
      f.input :town
      f.input :county
      f.input :postcode
      f.input :occupation
      f.input :dob, as: :date_picker
      f.input :email
    end

    inputs 'Contact Details' do
      f.input :home_phone
      f.input :business_phone
      f.input :mobile_phone
    end

    inputs 'Membership Details' do
      f.input :membership_type, collection: %w(Full Associate)
      f.input :payment_method, collection: %w(Cash Standing\ Order Cheque)
      f.input :paid_in_full, collection: %w(Yes No)
      f.input :payment_last_recieved, as: :date_picker
    end

    inputs 'Membership Form' do
      f.input :file, as: :file
    end
    f.actions
  end

  # Membership Index (All Memberships)
  index do
    selectable_column
    column 'Membership No' do |i|
      i.id
    end
    column :forename
    column :surname
    column :membership_type
    column :payment_method
    column :paid_in_full
    column :payment_last_recieved
    column 'File' do |f|
      link_to('Download Membership Form', download_pdf_vandals_member_admin_login_memberships_path(id: f.id))
    end
    actions
  end

  # Show
  show do
    attributes_table do
      row 'Membership No' do |s|
        s.id
      end
      row :forename
      row :surname
      row :address
      row :town
      row :county
      row :postcode
      row :occupation
      row :dob
      row :home_phone
      row :business_phone
      row :mobile_phone
      row :email
      row :membership_type
      row :payment_method
      row :paid_in_full
      row :payment_last_recieved
      row 'File' do |f|
        link_to('Download Membership Form', download_pdf_vandals_member_admin_login_memberships_path(id: f.id))
      end
    end
  end

end
