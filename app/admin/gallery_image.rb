ActiveAdmin.register GalleryImage do
  # filters
  filter :with_tournament_id,
         as: :select,
         collection: -> { Tournament.pluck(:name, :id) },
         label: 'Tournament Name'

  # Gallery Index (All Galleries)
  index do
    selectable_column
    column 'Tournament' do |gallery_image|
      gallery_image.gallery.tournament.name
    end
    column 'Photo' do |p|
      image_tag(p.photo.url(:gallery_thumb))
    end
    column :photo_file_name
    column :photo_content_type
    column :photo_file_size
    actions
  end
end
