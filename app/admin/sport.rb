ActiveAdmin.register Sport do
  permit_params :name

# Create Tournament
  form do |f|
    inputs 'Sport Details' do
      f.semantic_errors
      f.input :name
      f.actions
    end
  end

  # Sport Index (All Sports)
  index do
    selectable_column
    column :name
    actions
  end

  # Show
  show do
    attributes_table do
      row :name
    end
  end


end
