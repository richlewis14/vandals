ActiveAdmin.register Gallery do
  permit_params :tournament_id, gallery_images_attributes: [:id, :photo]

  # filters
  filter :tournament_id,
         as: :select,
         collection: Tournament.all

  # Create Gallery
  form html: { multipart: true } do |f|
    inputs 'Gallery Details' do
      f.input :tournament_id, as: :select, collection: Tournament.all
      f.has_many :gallery_images do |g|
        g.input :photo, as: :file, :hint => g.object.photo.present? ? image_tag(g.object.photo.url(:gallery_thumb)) : content_tag(:span, 'No photo chosen yet')
      end
      f.actions
    end
  end

  # Gallery Index (All Galleries)
  index do
    selectable_column
    column :tournament_id do |t|
      t.tournament.name
    end
    column 'Photos' do |p|
      photos = p.gallery_images.map
      photos.each do |photo|
        li do
          image_tag(photo.photo.url(:gallery_thumb))
        end
      end
    end
    actions
  end

  # Show
  show do
    attributes_table do
      row :tournament_id do |t|
        t.tournament.name
      end
      row 'Photos' do |p|
        photos = p.gallery_images.map
        photos.each do |photo|
          li do
            image_tag(photo.photo.url(:gallery_thumb))
          end
      	end
      end
    end
  end
end
