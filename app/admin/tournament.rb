ActiveAdmin.register Tournament do
  permit_params :name, :tourn_date, :tourn_location, :sport_id
  config.filters = false

  # Create Tournament
  form do |f|
    inputs 'Tournament Details' do
      f.semantic_errors
      f.input :sport_id, as: :select, collection: Sport.all
      f.input :name
      f.input :tourn_date, as: :date_picker
      f.input :tourn_location
      f.actions
    end
  end

  # Tournament Index (All Tournaments)
  index do
    selectable_column
    column :sport_id do |s|
      s.sport.name
    end
    column :name
    column :tourn_date
    column :tourn_location
    actions
  end

  # Show
  show do
    attributes_table do
      row :sport_id do |s|
        s.sport.name
      end
      row :name
      row :tourn_date
      row :tourn_location
    end
  end
end
