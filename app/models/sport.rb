class Sport < ActiveRecord::Base
  has_many :fixtures
  has_many :tournaments
end
