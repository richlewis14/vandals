# Membership Class
class Membership < ActiveRecord::Base
  has_attached_file :file
  validates_attachment :file, content_type: { :content_type => ['application/pdf'], message: 'PDF must be pdf format only' },
                              size: { in: 0..2000.kilobytes, message: "PDF's should be less than 2 megabytes" }
end
