include S3ImageConfig
# Gallery Image Class
class GalleryImage < ActiveRecord::Base
  belongs_to :gallery
  has_attached_file :photo,
                styles: {
                  gallery_thumb: '100x100#',
                  gallery_large: '400x400#',
                  gallery_small: '215x215#'
                }

  def self.ransackable_scopes(auth_object = nil)
    [:with_tournament_id]
  end

  scope :with_tournament_id, ->(tournament_id) { Tournament.find(tournament_id).gallery_images }

  validates_attachment :photo, content_type: { :content_type => ['image/jpg', 'image/jpeg', 'image/gif', 'image/png'], message: 'Images must be jpg/jpeg/gif/png format only' },
                               size: { in: 0..2000.kilobytes, message: 'Images should be less than 2 megabytes' }
end

