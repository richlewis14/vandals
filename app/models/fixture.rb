# Fixture Model
class Fixture < ActiveRecord::Base
  belongs_to :tournament
  belongs_to :sport
end
