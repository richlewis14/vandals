# Tournament Model
class Tournament < ActiveRecord::Base
  has_many :fixtures, dependent: :destroy
  has_one :gallery, dependent: :destroy
  has_many :gallery_images, through: :gallery
  belongs_to :sport
end
