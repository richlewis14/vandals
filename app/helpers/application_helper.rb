module ApplicationHelper

def date_output(date)
    date.strftime('%e %B %Y') if date
  end

  def time_output(time)
    time.strftime('%H:%M') if time
  end

  def youtube_embed(youtube_url)
  if youtube_url[/youtu\.be\/([^\?]*)/]
    youtube_id = $1
  else
    # Regex from # http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url/4811367#4811367
    youtube_url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
    youtube_id = $5
  end

  %Q{<iframe title="YouTube video player" width="583" height="317px" src="https://www.youtube.com/embed/#{ youtube_id }" frameborder="0" allowfullscreen></iframe>}
end

  #Get dimensions of Image retrieved from facebook, if too large we are not going to display it
  def image_dimension(url)
    geometry = Paperclip::Geometry.from_file(url)
    width = geometry.width.to_i 
    height = geometry.height.to_i
  end

end