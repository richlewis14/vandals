desc 'This task is called by the Heroku scheduler add-on'
task facebook: :environment do
    f = FacebookFeed.new
    f.get_feed
end
